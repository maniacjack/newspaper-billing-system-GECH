-- MySQL dump 10.13  Distrib 5.6.30, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: newspaper
-- ------------------------------------------------------
-- Server version	5.6.30-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `paper` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `day` varchar(20) DEFAULT NULL,
  `delivery` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES ('prajavani','2016-10-28','sat','yes'),('vijaya karnataka','2016-10-28','sat','yes'),('kannada prabha','2016-10-28','sat','yes'),('samyuktha karnataka','2016-10-28','sat','yes'),('janamitra','2016-10-28','sat','no'),('vijaya vani','2016-10-28','sat','no'),('Indian express','2016-10-28','sat','no'),('the hindu','2016-10-28','sat','yes'),('times of India','2016-10-28','sat','yes'),('prajavani','2016-10-28','mon','yes'),('vijaya karnataka','2016-10-28','mon','no'),('kannada prabha','2016-10-28','mon','no'),('samyuktha karnataka','2016-10-28','mon','no'),('janamitra','2016-10-28','mon','no'),('vijaya vani','2016-10-28','mon','no'),('Indian express','2016-10-28','mon','no'),('the hindu','2016-10-28','mon','no'),('times of India','2016-10-28','mon','yes'),('prajavani','2016-10-28','sat','yes'),('vijaya karnataka','2016-10-28','sat','yes'),('kannada prabha','2016-10-28','sat','yes'),('samyuktha karnataka','2016-10-28','sat','yes'),('janamitra','2016-10-28','sat','yes'),('vijaya vani','2016-10-28','sat','yes'),('Indian express','2016-10-28','sat','no'),('the hindu','2016-10-28','sat','no'),('times of India','2016-10-28','sat','no'),('prajavani','2016-10-28','wed','yes'),('vijaya karnataka','2016-10-28','wed','yes'),('kannada prabha','2016-10-28','wed','yes'),('samyuktha karnataka','2016-10-28','wed','yes'),('janamitra','2016-10-28','wed','yes'),('vijaya vani','2016-10-28','wed','yes'),('Indian express','2016-10-28','wed','no'),('the hindu','2016-10-28','wed','no'),('times of India','2016-10-28','wed','no');
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cost`
--

DROP TABLE IF EXISTS `cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cost` (
  `slno` int(11) NOT NULL,
  `paper` varchar(100) DEFAULT NULL,
  `day` varchar(20) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cost`
--

LOCK TABLES `cost` WRITE;
/*!40000 ALTER TABLE `cost` DISABLE KEYS */;
INSERT INTO `cost` VALUES (1,'prajavani','sun',4),(2,'prajavani','mon',4),(3,'prajavani','tue',4),(4,'prajavani','wed',4),(5,'prajavani','thu',4),(6,'prajavani','fri',4),(7,'prajavani','sat',4),(8,'vijaya karnataka','sun',4),(9,'vijaya karnataka','mon',4),(10,'vijaya karnataka','tue',4),(11,'vijaya karnataka','wed',4),(12,'vijaya karnataka','thu',4),(13,'vijaya karnataka','fri',4),(14,'vijaya karnataka','sat',4),(15,'kannada prabha','sun',4),(16,'kannada prabha','mon',4),(17,'kannada prabha','tue',4),(18,'kannada prabha','wed',4),(19,'kannada prabha','thu',4),(20,'kannada prabha','fri',4),(21,'kannada prabha','sat',4),(22,'samyuktha karnataka','sun',4),(23,'samyuktha karnataka','mon',4),(24,'samyuktha karnataka','tue',4),(25,'samyuktha karnataka','wed',4),(26,'samyuktha karnataka','thu',4),(27,'samyuktha karnataka','fri',4),(28,'samyuktha karnataka','sat',4),(29,'janamitra','sun',4),(30,'janamitra','mon',4),(31,'janamitra','tue',4),(32,'janamitra','wed',4),(33,'janamitra','thu',4),(34,'janamitra','fri',4),(35,'janamitra','sat',4);
/*!40000 ALTER TABLE `cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `d1` date DEFAULT NULL,
  `d2` date DEFAULT NULL,
  `pj` int(11) DEFAULT NULL,
  `vk` int(11) DEFAULT NULL,
  `kp` int(11) DEFAULT NULL,
  `sk` int(11) DEFAULT NULL,
  `jm` int(11) DEFAULT NULL,
  `vv` int(11) DEFAULT NULL,
  `ie` int(11) DEFAULT NULL,
  `th` int(11) DEFAULT NULL,
  `ti` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES ('2016-10-27','2016-10-27',5,5,8,8,5,5,8,8,5);
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-28 19:19:49
